from __future__ import annotations

import importlib
import shutil
import subprocess
import sys
from argparse import ArgumentParser
from contextlib import contextmanager
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Iterator
from typing import Optional

import nox

nox.options.sessions = ["lint"]
nox.options.reuse_existing_virtualenvs = True

PACKAGE_NAME = "pawapi"
VERSION_MODULE = "version"

PYPI_PROJECT_NAME = "pypawapi"
RELEASE_BRANCH = "master"

PYTHON_VERSION = "3.10"
PYTHON_ALL_VERSIONS = ["3.7", "3.8", "3.9", "3.10", "3.11"]
SOURCE_FILES = [f"src/{PACKAGE_NAME}", "noxfile.py", "tests"]

VENV_PATH = Path.cwd() / ".venv"
DIST_PATH = Path.cwd() / "dist"


@nox.session(python=False)
def dev(session: nox.Session) -> None:
    """ Create dev environment """

    python = str(VENV_PATH / "bin/python")
    session.run_always(
        "virtualenv", "--python", PYTHON_VERSION, str(VENV_PATH),
        silent=True,
        external=True,
    )  # yapf: disable
    session.run_always(
        python, "-m", "pip", "install", "--upgrade", "pip",
        external=True,
        silent=True,
    )  # yapf: disable
    session.run_always(
        python, "-m", "pip", "install", "-e", ".[dev]",
        external=True,
        silent=True,
    )  # yapf: disable


@nox.session(python=PYTHON_VERSION)
def lint(session: nox.Session) -> None:
    upgrade_pip(session)
    session.install("-e", ".[lint]")
    session.run("flake8", *SOURCE_FILES)
    session.run("isort", "--check-only", *SOURCE_FILES)
    session.run("yapf", "--parallel", "--quiet", "--recursive", *SOURCE_FILES)
    session.run("mypy", SOURCE_FILES[0])


@nox.session(python=PYTHON_ALL_VERSIONS)
def tests(session: nox.Session) -> None:
    upgrade_pip(session)
    session.install("-e", ".[test]")
    session.run(
        "pytest",
        "--strict-config",
        f"--cov={PACKAGE_NAME}",
        "--cov-report=",
    )
    session.notify("coverage")


@nox.session(python=PYTHON_VERSION)
def coverage(session: nox.Session) -> None:
    upgrade_pip(session)
    session.install("coverage")
    try:
        session.run("coverage", "report")
    finally:
        session.run("coverage", "erase", silent=True)


@nox.session(python=PYTHON_VERSION, name="prepare-release")
def prepare_release(session: nox.Session) -> None:
    check_current_branch(session, RELEASE_BRANCH)

    parser = ArgumentParser()
    parser.add_argument("version", choices=["major", "minor", "patch"])
    args = parser.parse_args(session.posargs)

    if git_has_uncommitted_changes("--staged"):
        session.error("Has uncommitted changes")

    version = project_get_version()
    if not project_version_is_valid(version):
        session.error("Invalid version format")

    version_file = VERSION_MODULE.replace(".", "/") + ".py"
    file_path = Path("./src") / PACKAGE_NAME / version_file

    new_version = project_bump_version(file_path, args.version)
    if new_version is None:
        session.error("Can't update project version")
    if not git_commit_file(str(file_path), "Bump version"):
        session.error("Can't commint new version")

    git_add_tag(f"Release {new_version}", new_version)


@nox.session(python=PYTHON_VERSION, name="build-release")
def build_release(session: nox.Session) -> None:
    check_current_branch(session, RELEASE_BRANCH)

    session.install("build")
    if DIST_PATH.exists():
        session.error("/dist already exists! Delete and try again")

    parser = ArgumentParser(prog="nox -s build-release --")
    parser.add_argument("version", default=None, nargs="?")
    args = parser.parse_args(session.posargs)

    version = args.version
    if version is None:
        version = project_get_version()
    if not project_version_is_valid(version):
        session.error("Invalid version format")
    if version not in git_get_tags():
        session.error(f"Version {version} doesn't exist")

    session.log(f"Creating isolated environment for version {version}")
    with isolated_checkout(version) as tmprepo:
        with session.chdir(tmprepo):
            session.run("pyproject-build")

        tmpdist = tmprepo / "dist"
        files = [x.name for x in tmpdist.glob("*")]
        if not check_generated_files(files, version):
            session.error("Got wrong files")

        DIST_PATH.mkdir()
        for src, dest in [[d / f for d in (tmpdist, DIST_PATH)] for f in files]:
            shutil.copy(src, dest)


@nox.session(python=PYTHON_VERSION, name="publish-release")
def publish_release(session: nox.Session) -> None:
    session.install("twine")
    if not DIST_PATH.exists():
        session.error(
            "/dist doesn't exist."
            "Use 'nox -s build-release' for build latest version"
        )

    dist_files = list(DIST_PATH.glob("*"))
    if not check_generated_files(files=[x.name for x in dist_files]):
        session.error(
            "Wrong files in /dist. "
            "Use 'nox -s build-release' for build latest version"
        )
    dist_files = [str(x) for x in dist_files]
    session.run("twine", "check", *dist_files)
    session.log("# Uploading to PyPi")
    session.run("twine", "upload", *dist_files, *session.posargs)


# Utils {{{1
def check_generated_files(
    files: Optional[list] = None,
    version: Optional[str] = None,
) -> bool:
    if version is None:
        version = project_get_version()
    if files is None:
        files = [x.name for x in DIST_PATH.glob("*")]

    package_name = PYPI_PROJECT_NAME.replace("-", "_")
    expected_files = sorted([
        f"{package_name}-{version}-py3-none-any.whl",
        f"{package_name}-{version}.tar.gz",
    ])
    return sorted(files) == expected_files


def upgrade_pip(session: nox.Session) -> None:
    session.run(
        "python", "-m", "pip", "install", "--upgrade", "pip",
        silent=True,
    )  # yapf: disable


def project_version_is_valid(version: str) -> bool:
    return len((v := version.split("."))) == 3 and all([x.isdigit() for x in v])


def project_get_version() -> str:
    sys.path.append(f"src/{PACKAGE_NAME}")
    version_module = importlib.import_module(
        VERSION_MODULE,
        package=PACKAGE_NAME,
    )
    return version_module.__version__


def project_bump_version(file: Path, section: str) -> str | None:
    version = project_get_version()
    new_version = _bump_version(version, section)
    if not _write_new_version(file, new_version):
        return None
    return new_version


def _write_new_version(file: Path, version: str) -> bool:
    with file.open("r") as f:
        lines = f.readlines()
    updated = False
    with file.open("w") as f:
        for row in lines:
            if row.startswith("__version__"):
                f.write(f'__version__ = "{version}"\n')
                updated = True
            else:
                f.write(row)
    return updated


def _bump_version(version: str, section: str) -> str:
    major, minor, patch = map(int, version.split("."))
    if section == "major":
        major += 1
        minor = patch = 0
    elif section == "minor":
        minor += 1
        patch = 0
    elif section == "patch":
        patch += 1

    return f"{major}.{minor}.{patch}"


def git_get_current_branch() -> str:
    return subprocess.run(
        ["git", "rev-parse", "--abbrev-ref", "HEAD"],
        check=True,
        capture_output=True,
        encoding="utf-8",
    ).stdout.strip()


def git_has_uncommitted_changes(*args) -> bool:
    return subprocess.run(
        ["git", "diff", "--no-patch", "--exit-code", *args],
        capture_output=True,
    ).returncode != 0


def git_add_tag(message: str, tag_name: str) -> None:
    subprocess.run(["git", "tag", "-m", message, tag_name], check=True)


def git_get_tags() -> list:
    return subprocess.run(
        ["git", "tag"],
        capture_output=True,
        encoding="utf-8",
    ).stdout.strip().splitlines()


def git_commit_file(file: str, message: str) -> bool:
    if subprocess.run(
        ["git", "add", file],
        capture_output=True,
    ).returncode == 0:  # yapf: disable
        return subprocess.run(
            ["git", "commit", "-m", message],
            capture_output=True,
        ).returncode == 0
    return False


def check_current_branch(session: nox.Session, release="master") -> str:
    if (current := git_get_current_branch()) != release:
        session.error(
            f"Currnet branch is {current!r}, "
            f"release branch is {release!r}!"
        )


@contextmanager
def isolated_checkout(version: str) -> Iterator[Path]:
    with TemporaryDirectory() as tmpdir_name:
        tmprepo_path = Path(tmpdir_name) / "build"
        subprocess.run(
            [
                "git", "clone",
                "--depth", "1",
                "--branch", version,
                "--",
                f"file://{Path.cwd()}", tmprepo_path,
            ],
            check=True,
            capture_output=True,
        )  # yapf: disable

        yield tmprepo_path


# }}}1
