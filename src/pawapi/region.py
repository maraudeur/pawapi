from enum import Enum


class Region(Enum):
    """Available regions"""

    US = "us"
    EU = "eu"
